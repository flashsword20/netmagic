package us.codecraft.netmagic;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;

/**
 * @author code4crafter@gmail.com <br>
 *         Date: 13-8-11 <br>
 *         Time: 下午3:16 <br>
 */
public class ResoucePage extends Page {

    public static final String FILE_PATH = "_filePath";

    public ResoucePage addTargetResouce(String url,String filePath){
        addTargetRequest(new Request(url).putExtra(FILE_PATH,"filePath"));
        return this;
    }
}
