package us.codecraft.netmagic.downloader;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.downloader.HttpClientDownloader;
import us.codecraft.webmagic.utils.FilePersistentBase;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author code4crafter@gmail.com <br>
 *         Date: 13-8-11 <br>
 *         Time: 下午3:37 <br>
 */
public class ResourceHttpClientDownloader extends HttpClientDownloader {

    public static final String FILE_PATH = "_filePath";

    private Logger logger = Logger.getLogger(getClass());

    private FilePersistentBase filePersistentBase = new FilePersistentBase();

    /**
     * 新建一个ResouceHttpClientDownloader，使用默认保存路径"/data/webmagic/"
     */
    public ResourceHttpClientDownloader() {
        filePersistentBase.setPath("/data/webmagic/resouce/");
    }

    /**
     * 新建一个FilePipeline
     *
     * @param path 文件保存路径
     */
    public ResourceHttpClientDownloader(String path) {
        filePersistentBase.setPath(path);
    }

    @Override
    protected Page handleResponse(Request request, String charset, HttpResponse httpResponse, Task task) throws IOException {
        Object extra = request.getExtra(FILE_PATH);
        if (extra == null) {
            return super.handleResponse(request, charset, httpResponse, task);
        } else {
            String path = this.filePersistentBase.getPath() + FilePersistentBase.PATH_SEPERATOR + task.getUUID() + FilePersistentBase.PATH_SEPERATOR;
            String fileName = path + extra.toString();
            FileOutputStream fileOutputStream = new FileOutputStream(filePersistentBase.getFile(fileName));
            IOUtils.copy(httpResponse.getEntity().getContent(), fileOutputStream);
            fileOutputStream.close();
            httpResponse.getEntity().getContent().close();
            return null;
        }
    }

}
